import Vue from 'vue'
import VueRouter from 'vue-router'

import Login from '@/views/admin/login/login.vue'
import Inscription from '@/views/admin/Inscription/Inscription.vue'
import Home from '@/views/Magasin/Home/Home.vue'
import Magasin from '@/views/Magasin/Magasin.vue'
import Management from '@/views/admin/management/management.vue'
import Categorie from '@/views/Magasin/Categorie/Categorie.vue'
import Produit from '@/views/Magasin/Produit/Produit.vue'
import Panier from '@/views/Magasin/Panier/Panier.vue'
import Commande from '@/views/Magasin/Commande/Commande.vue'
import CommandeSucces from '@/views/Magasin/Commande/CommandeSucces/CommandeSucces.vue'
import Contact from '@/views/Magasin/Contact/Contact.vue'

const routes = [
  {
    path: '*',
    redirect: '/home'
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/home',
    name: 'Home',
    component: Home
  },
  {
    path: '/',
    name: 'Magasin',
    component: Magasin,
    children: [
      {
        path: '',
        name: 'Home',
        component: Home
      },
      {
        path: '/categorie/:catId',
        name: 'Categorie',
        component: Categorie
      },
      {
        path: '/categorie/:catId/produit/:prodId',
        name: 'Produit',
        component: Produit
      },
      {
        path: '/panier',
        name: 'Panier',
        component: Panier
      },
      {
        path: '/commande',
        name: 'Commande',
        component: Commande
      },
      {
        path: '/commandeSucces',
        name: 'CommandeSucces',
        component: CommandeSucces
      },
      {
        path: '/contact',
        name: 'Contact',
        component: Contact
      }
    ]
  },
  {
    path: '/management',
    name: 'Management',
    component: Management,
    meta: {
      requiresAuth: true
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/inscription',
    name: 'Inscription',
    component: Inscription,
  },

]

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  history: true,
  routes
})

// Check auth before each routes
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check if user is loggin
    if (localStorage.getItem('token')) {
      // Get info about the user
        next()
        return
    } else {
      next('/login')
    }

  } else {
    next()
  }
})

export default router
