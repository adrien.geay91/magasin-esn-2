import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'
export default {
  data() {
    return {
      needRegister: false,
      loginForm: {
        email: '',
        password: ''
      },
      registerForm: {
        email: '',
        password: '',
        password2: ''
      },
      email: '',
      password: '',
      missingMail: false,
      error: false
    }
  },
  methods: {
    ...mapActions(['loginUser']),
    /**
     * Function to login
     */
    login() {
      const user = {
        email: this.email,
        password: this.password
      }
      this.loginUser(user).then(res => {
        this.$router.push('/management')
      }).catch(err => {
        this.error = true
      })
    },
    showRegister() {
      this.needRegister = true
    },
    showLogin() {
      this.needRegister = false
    }
  }
}