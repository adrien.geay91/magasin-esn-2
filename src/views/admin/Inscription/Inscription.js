import { mapActions } from 'vuex';
import InscriptionI18n from './Inscription.i18n.js';

export default {

  components: {},

  props: {},

  computed: {},

  data() {
    return {
      email: '',
      password: '',
      password2: '',
      isSucces: false
    };
  },

  watch: {},

  methods: {
    ...mapActions(['createUser']),
    enregistre() {
      const info = {
        email : this.email,
        password: this.password
      }
      this.createUser(info).then(res => {
        this.isSucces = true
      }).catch(err => {
        console.log(err)
      })
    }
  },

  mounted() {},

  i18n: InscriptionI18n,

};
