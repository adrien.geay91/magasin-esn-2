import ContactI18n from './Contact.i18n.js';

export default {

  components: {},

  props: {},

  data() {
    return {};
  },

  watch: {},

  methods: {},

  mounted() {},

  i18n: ContactI18n,

};
