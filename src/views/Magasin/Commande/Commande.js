import CommandeI18n from './Commande.i18n.js';
import {
  mapGetters,
  mapMutations,
  mapActions,
} from 'vuex'
export default {

  components: {},
  computed: {
    ...mapGetters(['cart'])
  },
  props: {},

  data() {
    return {
      name: '',
      email: '',
      phone: '',
      adress: '',
      CP: '',
      city: '',
      comments: '',
      error: false
    };
  },

  watch: {},

  methods: {
    ...mapMutations(['clearCart']),
    ...mapActions(['orderCart']),
    /**
     * Function to open the order modal.
     */
    openModalOrder() {
      // Check fields
      if (!this.name || this.name === '' || !this.email || this.email === '' || !this.phone || this.phone === '' ||
        !this.adress || !this.adress === '' || !this.CP || !this.CP === '' || !this.city || !this.city === '') {
        this.error = true
      } else {
        this.error = false
        this.$refs['confirm-modal'].show()

      }
    },
    /**
     * Function to close the order modal.
     */
    closeModalOrder() {
      this.$refs['confirm-modal'].hide()
    },
    /**
     * Function to send the order.
     */
    order() {
      const orderId = Math.floor(Math.random() * Math.floor(new Date().getTime())) + "a"
      const order = {
        name: this.name,
        email: this.email,
        phone: this.phone,
        adress: this.adress,
        CP: this.CP,
        city: this.city,
        comments: this.comments,
        cart: this.cart,
        id: orderId
      }
      this.orderCart(order).then(res => {
        // Clear the cart
        this.clearCart()
        this.$router.push({
          path: `/commandeSucces`
        })
      }).catch(err => {
        console.log(err)
      })
    },
    /**
     * Function to go to cart.
     */
    goToCart() {
      this.$router.push({
        path: `/panier`
      })
    }
  },

  mounted() {},

  i18n: CommandeI18n,

};
