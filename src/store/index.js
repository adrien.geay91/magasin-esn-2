import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'


Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    categories: [],
    families: [],
    products: [],
    cart: [],
    orders: [],
    dataLoading: false,
    dataUploading: false,
    payed: 0
  },
  actions: {
    deleteCategory({
      commit
    }, category) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataLoading', true)
        axios.delete('http://localhost:3000/api/categories/' + category.id, {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setDataLoading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataLoading', false)
          reject(error)
        });
      });
    },

    deleteProduct({
      commit
    }, category) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataLoading', true)
        axios.delete('http://localhost:3000/api/produits/' + category.id, {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setDataLoading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataLoading', false)
          reject(error)
        });
      });
    },
    /**
     * Service to get the categories.
     * @param {*} param0 
     */
    getCategories({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        commit('setDataLoading', true)
        axios.get('http://localhost:3000/api/categories').then(response => {
          commit('setCategories', response.data)
          commit('setDataLoading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataLoading', false)
          reject(error)
        });
      });
    },


    /**
     * Service to add a category.
     * @param {*} param0 
     */
    addCategory({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataUploading', true)
        axios.post('http://localhost:3000/api/categories', info, {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setDataUploading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to add a category.
     * @param {*} param0 
     */
    updateCategory({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataUploading', true)
        axios.put('http://localhost:3000/api/categories/' + info.id, info, {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setDataUploading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to add a product.
     * @param {*} param0 
     */
    addProduct({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataUploading', true)
        axios.post('http://localhost:3000/api/produits', info, {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setDataUploading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to update a product.
     * @param {*} param0 
     */
    updateProduct({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataUploading', true)
        axios.put('http://localhost:3000/api/produits/' + info.id, info, {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setDataUploading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to get the products.
     * @param {*} param0 
     */
    getProductsByCategory({
      commit
    }, catId) {
      return new Promise((resolve, reject) => {
        commit('setDataLoading', true)
        axios.get('http://localhost:3000/api/categories/' + catId + '/produits').then(response => {
          commit('setProducts', response.data)
          commit('setDataLoading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataLoading', false)
          reject(error)
        });
      });
    },


    /**
     * Service to get all the products.
     * @param {*} param0 
     */
    getProducts({
      commit
    }, catId) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataLoading', true)
        axios.get('http://localhost:3000/api/produits', {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setProducts', response.data)
          commit('setDataLoading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataLoading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to add a product.
     * @param {*} param0 
     */
    orderCart({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        commit('setDataUploading', true)
        axios.post('http://localhost:3000/api/commandes', info).then(response => {
          commit('setDataUploading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to get all the orders.
     * @param {*} param0 
     */
    getOrders({
      commit
    }, catId) {
      return new Promise((resolve, reject) => {
        const token = localStorage.getItem('token')
        const AuthStr = 'Bearer ' + token
        commit('setDataLoading', true)
        axios.get('http://localhost:3000/api/commandes', {
          headers: {
            Authorization: AuthStr
          }
        }).then(response => {
          commit('setOrders', response.data)
          commit('setDataLoading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataLoading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to create an user a product.
     * @param {*} info 
     */
    createUser({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        commit('setDataUploading', true)
        axios.post('http://localhost:3000/api/users', info).then(response => {
          commit('setDataUploading', false)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

    /**
     * Service to login.
     * @param {*} info 
     */
    loginUser({
      commit
    }, info) {
      return new Promise((resolve, reject) => {
        commit('setDataUploading', true)
        axios.post('http://localhost:3000/api/login', info).then(response => {
          commit('setDataUploading', false)
          localStorage.setItem('token', response.data.token)
          resolve(response)
        }).catch(error => {
          commit('setDataUploading', false)
          reject(error)
        });
      });
    },

  },
  mutations: {
    setCategories(state, val) {
      state.categories = val
    },
    setFamilies(state, val) {
      state.families = val
    },
    setProducts(state, val) {
      state.products = val
    },
    setDataLoading(state, val) {
      state.dataLoading = val
    },
    setDataUploading(state, val) {
      state.dataUploading = val
    },
    setCart(state, cart) {
      state.cart = cart
    },
    setOrders(state, orders) {
      state.orders = orders
    },
    addToCart(state, product) {
      state.cart.push(product)
    },
    deleteFromCart(state, product) {
      state.cart.splice(state.cart.indexOf(product), 1)
    },
    setTotalProductPrice(state, product) {
      state.cart[state.cart.indexOf(product)].totalProductPrice = (product.quantity * parseFloat(product.price).toFixed(2)).toFixed(2)
    },
    clearCart(state) {
      state.cart = []
    },
    setPayed(state, payed) {
      state.payed = payed
    }
  },
  getters: {
    categories: state => state.categories,
    families: state => state.families,
    products: state => state.products,
    cart: state => state.cart,
    orders: state => state.orders,
    dataLoading: state => state.dataLoading,
    dataUploading: state => state.dataUploading,
    payed: state => state.payed
  },
})